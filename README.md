# ProtonMail Beta

A basic userscript to automatically switch to ProtonMail's beta subdomain by redirecting _mail.protonmail.com_ to _beta.protonmail.com_.
